using Godot;
using System;

public class EnemyDyingActionState : EnemyActionState
{
	public EnemyDyingActionState(Enemy Enemy)
	{
		this.Enemy = Enemy;
		Enemy.PlayAnim("Die");
		Enemy.GetNode<AudioData>("Sounds/Die").StartPlaying = true;
		if (Enemy.Boss)
		{
			Enemy.EmitSignal(nameof(Enemy.Died), true, Enemy.Position);
		}
		//Enemy is QueueFreed after the die animation is finished (see ENEMY.CS)
	}
	
	public override void Update(float delta)
	{
		base.Update(delta);
	  
	}

	public override void _Process(float delta)
	{
		
	}
}
