using Godot;
using System;

public class Enemy : KinematicBody2D
{
	[Export] public int Speed = 0;
	[Export] public int MaxSpeed = 200;
	[Export] public int Health = 40;
	[Export] public int MaxHealth = 40;
	[Export] public int SpitSpeed = 1000;
	public Vector2 Direction = new Vector2();
	public AnimationPlayer Anim;
	public RayCast2D RayCastDownRight;
	public RayCast2D RayCastDownLeft;
	public RayCast2D RayCastLeft;
	public RayCast2D RayCastRight;
	//public Label Label;

	public bool MoveRight;
	public bool MoveLeft;
/*     [Signal] public delegate void LookingForPlayer();  */

	[Signal]
	public delegate void OnLookingForPlayer(Enemy e);
	[Signal]
	public delegate void Attacking(PackedScene shootScn, Vector2 pos, Vector2 lastKnownPlayerPos);
	[Signal]
	public delegate void AttackingBoss(PackedScene shootScn, Vector2 pos, Vector2 lastKnownPlayerPos);//NEW

	[Signal]
	public delegate void Died(bool boss, Vector2 position);
 /*    public Vector2 PlayerPos; */
	//public Vector2 LastKnownPlayerPos = new Vector2();
	public Vector2 LastKnownPlayerPosition;
	public Timer SpitTimer;
	public Timer ChaseTimer;
	public bool SpawnSpit = false;
   // public bool ChasePlayer;
	 public int SpitWaitTimeMin = 1;
	 public int SpitWaitTimeMax = 3;
	public Sprite Sprite;
	TextureProgress TextureProgress;
	public Position2D Position2D;
	Area2D Area2D;
	public EnemyActionState EnemyActionState{get; set;}
	[Export]
	public PackedScene Spit = GD.Load<PackedScene>("res://Actors/Enemy/Spit.tscn");
	[Export]
	public PackedScene BossShoot = GD.Load<PackedScene>("res://Actors/Boss/BossShoot.tscn");
	public PackedScene BossShoot3 = GD.Load<PackedScene>("res://Actors/Boss/BossShoot3.tscn");
	[Export] public bool Boss = false;
	public float Damage = 4;
	String StateString;
	//CollisionPolygon2D CollisionPolygon2D;

	public Vector2 StartPosition {get; set;}

	public override void _Ready()
	{
		StartPosition = Position;
		Anim = GetNode<AnimationPlayer>("Anim");
		RayCastDownRight = GetNode<RayCast2D>("RayCastDownRight");
		RayCastDownLeft = GetNode<RayCast2D>("RayCastDownLeft");
		RayCastRight= GetNode<RayCast2D>("RayCastRight");
		RayCastLeft = GetNode<RayCast2D>("RayCastLeft");
		Position2D = GetNode<Position2D>("Position2D");
		SpitTimer= GetNode<Timer>("SpitTimer");
		ChaseTimer= GetNode<Timer>("ChaseTimer");
		Area2D = GetNode<Area2D>("Area2D");
		Sprite = GetNode<Sprite>("Sprite");
		TextureProgress = GetNode<TextureProgress>("HealthDisplay/TextureProgress");
		TextureProgress.MaxValue = MaxHealth;
		AddBossParams();
	//	Label = GetNode<Label>("Label");
		//CollisionPolygon2D = GetNode<CollisionPolygon2D>("CollisionPolygon2D");
		ChangeState(State.Patrolling);
		
	}


	public void AddBossParams() //new hacky
	{
		if (Boss)
		{
			Health = 1000;
			MaxHealth = 1000;
			Damage = 10;	
		}
	}

	public bool IsWallOnRight()
	{
		if (RayCastRight.GetCollider() is TileMap || RayCastRight.GetCollider() is StaticBody2D || RayCastRight.GetCollider() is Player)
		{
			//GD.Print("Is Wall On Right is true");
			return true;
		}
		return false;
		
	}

	public bool IsWallOnLeft()
	{
		if (RayCastLeft.GetCollider() is TileMap || RayCastLeft.GetCollider() is StaticBody2D || RayCastLeft.GetCollider() is Player) 
		{
			//GD.Print("Is Wall On left is true");
			return true;
		}
		return false;
		
	}

	public bool IsFloorOnRight()
	{
		if (RayCastDownRight.GetCollider() is TileMap || RayCastDownRight.GetCollider() is StaticBody2D)
		{
			//GD.Print("Is floor On Right is true");
			return true;
			
		}
		return false;
		
	}

	public bool IsFloorOnLeft()
	{
		if (RayCastDownLeft.GetCollider() is TileMap || RayCastDownLeft.GetCollider() is StaticBody2D)
		{
		//	GD.Print("Is floor On left is true");
			return true;
			
		}
/* 		if (RayCastDownLeft.GetCollider() is null)
		{
			return false;
		} */
		return false;
	}

	public enum State
	{
		Patrolling,
		Attacking,
		Jumping,
		Dying
	}

	public void ChangeState(State state)
	{
		if (EnemyActionState != null)
		{
		}
		
		switch(state)
		{
			case State.Patrolling:
				EnemyActionState = new EnemyPatrollingActionState(this);
				StateString = "Patrol";
				break;
			case State.Attacking:
				EnemyActionState = new EnemyAttackingActionState(this);
				StateString = "attacking";
				break;
			case State.Jumping:
				EnemyActionState = new EnemyJumpingActionState(this);
				StateString = "Jumping";
				break;
			case State.Dying:
				EnemyActionState = new EnemyDyingActionState(this);
				StateString = "Dying";
				break;
		}
		
		
	}

	public State GetState()
	{
		if (EnemyActionState is EnemyPatrollingActionState)
		{
			return State.Patrolling;
		}
		else if (EnemyActionState is EnemyAttackingActionState)
		{
			return State.Attacking;
		}
		else if (EnemyActionState is EnemyJumpingActionState)
		{
			return State.Jumping;
		}
		else
		{
			return State.Dying;
		}
	}


	public override void _Process(float delta)
	{
		if(Health<MaxHealth)
		{
			TextureProgress.Show();
			
		}
		TextureProgress.Value = Health;
		
		EnemyActionState.Update(delta);
	//	Label.Text = StateString;
	   // EmitSignal(nameof(LookingForPlayer));      
	}

/*     public void OnPlayerFound(Vector2 _playerPos) //Enemy recieves player locaton
	{
		   PlayerPos = _playerPos;  
	} */

	public void OnSpitTimerTimeout()
	{
		//increase the wait time to reduce the spawning rate
		SpawnSpit = true;
		Random random = new Random();
		SpitTimer.WaitTime = random.Next(SpitWaitTimeMin, SpitWaitTimeMax);
		SpitTimer.Start();
	}

	public void OnChaseTimerTimeout()
	{
	}

	public void OnBodyArea2dBodyEntered(object body) //This is the area that recieves damage input
	{
		if (body is Player player)
		{
			player.SubtractFromHealth(Damage);
		}
		
	}


	public void TakeDamage(float num)
	{
		PlayAnim("Hurt");
		Health -= (int)num;
		if (Health < 1)
		{
			ChangeState(State.Dying);
		}
		else
		{
			GetNode<AudioData>("Sounds/Hurt").StartPlaying = true;
		}
	}

	public void OnBodyEntered(KinematicBody2D body)
	{
		if (body is Player)
		{
			//GD.Print("body is Player");
			GetNode<AudioData>("Sounds/Engage").StartPlaying = true;
			ChangeState(State.Attacking);
		}
	   
	}

	public void OnBodyExited(KinematicBody2D body)
	{
		if (body is Player)
		{
			ChangeState(State.Patrolling);
		}
	}

	public void PlayAnim(string _anim)
	{
		if (Anim.CurrentAnimation == _anim)
		{
			return;
		}
		if (_anim == "Die")
		{
			Anim.Play("Die");
			return;
		}
		if (_anim == "Hurt")
		{
			Anim.Play("Hurt");
		}
		if (Anim.IsPlaying())
		{
			return;
		}
		Anim.Play(_anim);
		// GD.Print(_anim);
	}

	public async void OnAnimFinished(string AnimName)
	{
		if (AnimName == "Die")
		{
			if (GetNode<AudioData>("Sounds/Die").Playing())
			{
				await ToSignal(GetNode<AudioData>("Sounds/Die"), nameof(AudioData.Finished)); 
			}
			//GD.Print("Die finsihed");
			QueueFree();

		} 
	}

	public void FlipRight()
	{
		if (Sprite.FlipH == true)
		{
			Sprite.FlipH = false;
		}
	}
	public void FlipLeft()
	{
		if (Sprite.FlipH == false)
		{
			Sprite.FlipH = true;
		}
	}
	
	
	public void LookForPlayer()
	{
		EmitSignal(nameof(Enemy.OnLookingForPlayer), this);
	}

	public void UpdatePlayerPosition(Vector2 pos)
	{
		//GD.Print(pos);
		LastKnownPlayerPosition = pos;
		//GD.Print(LastKnownPlayerPos + "Last known player pos");
	}
}
	
	

