using Godot;
using System;

public class TestPlayer : KinematicBody2D
{
[Export] public int speed = 500;
//[Signal] public delegate void OnPlayerFound(); 
public Vector2 SendPlayerPos;
	public Vector2 velocity = new Vector2();    

	public void GetInput()
	{
		velocity = new Vector2();

		if (Input.IsActionPressed("Right"))
			velocity.x += 1;

		if (Input.IsActionPressed("Left"))
			velocity.x -= 1;

		if (Input.IsActionPressed("Down"))
			velocity.y += 1;

		if (Input.IsActionPressed("Up"))
			velocity.y -= 1;

		velocity = velocity.Normalized() * speed;
	}
	
	public override void _Ready()
	{

	}

	public override void _PhysicsProcess(float delta)
	{
		GetInput();
		velocity = MoveAndSlide(velocity);
	}
//this method is accessed when the enemy is near the player. it transmits a signal that sends the player position to the enemy.
//connect this signal to the enemy
	public void OnLookingForPlayer() 
	{
		SendPlayerPos = this.GlobalPosition;
	   // EmitSignal(nameof(OnPlayerFound), SendPlayerPos);
	}
}
