using Godot;
using System;

public class EnemyAttackingActionState : EnemyActionState
{
  
	bool MoveRight;
	bool MoveLeft;
	bool FloorRight;
	bool FloorLeft;
	bool PlayerIsOnRight;


	
	public EnemyAttackingActionState(Enemy Enemy)
	{
		this.Enemy = Enemy;
		if (Enemy.SpitTimer.IsStopped())
		{
			Enemy.SpitTimer.Start();
		}

	}

	public override void Update(float delta)
	{
		base.Update(delta);
		Shoot();
		ChasePlayer(delta);
		
	}

	public override void _Process(float delta)
	{
		
	}


	public void Shoot()
	{
	   // Enemy.EmitSignal(nameof(Enemy.LookForPlayer));  
	   Enemy.LookForPlayer();
		if (Enemy.SpawnSpit == true)
		{
			Enemy.GetNode<AudioData>("Sounds/Attack").StartPlaying = true;
			Enemy.SpawnSpit = false;
			Enemy.EmitSignal(nameof(Enemy.Attacking), Enemy.Boss ? Enemy.BossShoot : Enemy.Spit,
				Enemy.Position2D.GlobalPosition, Enemy.LastKnownPlayerPosition);

			if (Enemy.Boss)
			{
				Enemy.EmitSignal(nameof(Enemy.AttackingBoss), Enemy.Spit, Enemy.Position2D.GlobalPosition, Enemy.LastKnownPlayerPosition); //new
			}
			//	Enemy.ChangeState(Enemy.State.Patrolling);
			// if (Enemy.Boss == true)
			// {
			// 	BossShoot BossShootInstance = (BossShoot)Enemy.BossShoot.Instance();
			// 	Enemy.Owner.AddChild(BossShootInstance);
			// 	var Pos = Enemy.Position2D.GlobalPosition;
			// 	BossShootInstance.Position = Pos;
			// 	BossShootInstance.LookAt(Enemy.LastKnownPlayerPosition);
			// }
			// if (Enemy.Boss == false)
			// {
			// 	Spit SpitInstance = (Spit)Enemy.Spit.Instance();
			// 	Enemy.Owner.AddChild(SpitInstance); 
			// 	var Pos = Enemy.Position2D.GlobalPosition;
			// 	SpitInstance.Position = Pos; //spawnpos
			// 	SpitInstance.LookAt(Enemy.LastKnownPlayerPosition);
			// }
			
		}   
		  
	}

	public void ChasePlayer(float delta)
	{

		   GetInput(delta); 
	}

	public void GetInput(float delta)
	{
		Enemy.Direction = new Vector2();
		Enemy.Speed = Enemy.MaxSpeed;
		//Vector2 LookAtPlayer = new Vector2(Enemy.LastKnownPlayerPosition.x, Enemy.Position.y);
		//Enemy.LookAt(LookAtPlayer);
		
	   if (Enemy.LastKnownPlayerPosition.x > Enemy.Position.x)
			{
				/* MoveRight = true;
				PlayerIsOnRight = true; */
				Enemy.FlipLeft();
			}
		if (Enemy.LastKnownPlayerPosition.x < Enemy.Position.x)
			{
				/* MoveRight = false;
				PlayerIsOnRight = false; */
				Enemy.FlipRight();
			}
/*          if (Enemy.IsFloorOnLeft() == false && MoveRight==false && PlayerIsOnRight == false)
		{
		} */
/* 		if (Enemy.IsWallOnRight())
		{
			MoveRight = false;
		}
		 if (Enemy.IsWallOnLeft())
		{
			MoveRight = true;     
		}
		if (Enemy.IsFloorOnRight() == false)
		{
			MoveRight = false;
		}
		if (Enemy.IsFloorOnLeft() == false)
		{
			MoveRight = true;  
		} */
/* 		if (MoveRight == true)
		{
			Enemy.PlayAnim("Patrolling");
			Enemy.Direction.x += 1;
			Enemy.Direction.y += 1;
		}
		if (MoveRight == false)
		{
			Enemy.PlayAnim("Patrolling");
			Enemy.Direction.x -= 1;
			Enemy.Direction.y += 1;
		} */
		//Enemy.MoveAndSlide(Enemy.Direction.Normalized() * Enemy.Speed);


	}
}
