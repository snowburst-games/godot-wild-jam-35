using Godot;
using System;

public class Spit : Area2D
{
   [Export] int _speed = 1100;
	Vector2 _target;
	public Vector2 Velocity = new Vector2();
	Timer _timer;

	[Export]private float dmg = 3f; // change depending on how strong the spit is
	
	public override void _Ready()
	{
		//_speed = Enemy.SpitSpeed;
		_timer = GetNode<Timer>("Timer");
		_timer.Start();
	}

	public override void _PhysicsProcess(float delta)
	{
		Position += Transform.x *_speed * delta;      
	}

	public void OnSpitBodyEntered(Node body)
	{
	   if (body is TileMap)
	   {
	   }

	   if (body is Player player)
	   {
		   player.SubtractFromHealth(dmg);
	   }

	   // It should die on entering any non enemy body
	   if (body is Enemy enemy)
	   {
		   return;
	   }
	   QueueFree();
		
	}

	public void OnTimerTimeout()
	{
		QueueFree();
	}

}
