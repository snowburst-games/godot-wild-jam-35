using Godot;
using System;

public class EnemyActionState : Node
{
    public Enemy Enemy;
 /*    bool MoveRight;
    bool MoveLeft; */
    bool FloorRight;
    bool FloorLeft;
    

    public Enemy.State NextState;

    public virtual void Update(float delta)
    {
       // Enemy.Direction.y += 1;
                Enemy.Direction = new Vector2();
        Enemy.Speed = Enemy.MaxSpeed;
       
        if (Enemy.IsWallOnRight())
        {
            Enemy.MoveRight = false;
        }
         if (Enemy.IsWallOnLeft())
        {
            Enemy.MoveRight = true;     
        }
        if (Enemy.IsFloorOnRight() == false)
        {
            Enemy.MoveRight = false;
        }
        if (Enemy.IsFloorOnLeft() == false)
        {
            Enemy.MoveRight = true;  
        }
        if (Enemy.MoveRight == true)
        {
            Enemy.PlayAnim("PatrollingRight");
            Enemy.Direction.x += 1;
            Enemy.Direction.y += 1;
        }
        if (Enemy.MoveRight == false)
        {
            Enemy.PlayAnim("PatrollingLeft");
            Enemy.Direction.x -= 1;
            Enemy.Direction.y += 1;
        }
         Enemy.MoveAndSlide(Enemy.Direction.Normalized() * Enemy.Speed);
    }

    public virtual void _Process(float delta)
    {
        
    }
}
