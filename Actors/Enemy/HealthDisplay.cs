using Godot;
using System;

public class HealthDisplay : Node2D
{
    TextureProgress TextureProgress;
    
    public override void _Ready()
    {
        TextureProgress = GetNode<TextureProgress>("TextureProgress");
        TextureProgress.Hide();
    }

    public override void _Process(float delta)
    {
        GlobalRotation = 0;
    }
}
