using Godot;
using System;

public class FallingActionState : ActionState
{
	public FallingActionState()
	{
		GD.Print("Error, using empty constructor");
	}

	public FallingActionState(Player player)
	{
		this.Player = player;
		if (Player.PreviousPlayerAction != Player.PlayerAction.Jumping)
		{
			Player.CoyoteTimer.Start();
		}
		Player.GetNode<Position2D>("AttackNozzle").Position = new Vector2(Player.GetNode<Position2D>("AttackNozzle").Position.x,
			-20);
		ShowIndividualSprite(Player.HasWeapon ? "SpriteStandDagger" : "SpriteStandNoDagger");
		// if (! Player.GetNode<AudioData>("Sounds/Walk").Playing())
		// {
		// 	Player.GetNode<AudioData>("Sounds/Walk").StopLastSoundPlayer();
		// }
		Player.PlayActionAnim("Fall");
	}

	public override void Update(float delta)
	{
		base.Update(delta);

		DoHorizontalMovement();
		DoAttack();

		// if (! Player.GetNode<AudioData>("Sounds/Walk").Playing())
		// {
		Player.GetNode<AudioData>("Sounds/Walk").StopLastSoundPlayer();
		// }

		if (Player.CoyoteTimer.TimeLeft > 0)
		{
			if (TryJumpStart)
			{
				Player.SetActionState(Player.PlayerAction.Jumping);
			}
		}

		if (Player.IsOnFloor())
		{
			Player.SetActionState(Player.PlayerAction.Idle);
		}

		
	}
}
