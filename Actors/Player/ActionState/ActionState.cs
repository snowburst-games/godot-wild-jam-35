using Godot;
using System;

public class ActionState : Reference
{
	
	public bool TryLeft {get; set;} = false;
	public bool TryRight {get; set;} = false;
	public bool TryJumpStart {get; set;} = false;
	public bool TryAttack {get; set;} = false;
	public bool TryLookUp {get; set;} = false;
	public bool TryCrouch {get; set;} = false;

	public Vector2 Velocity {get; set;}

	public Player Player {get; set;}

	private float _acceleration = 0.5f;
	private float _friction = 0.1f;

	private void GetInput()
	{
		TryLeft = Input.IsActionPressed("Left");
		TryRight = Input.IsActionPressed("Right");
		TryJumpStart = Input.IsActionPressed("Jump");
		if (TryJumpStart)
		{
			Player.JumpBufferTimer.Start();
		}
		TryAttack = Input.IsActionPressed("Attack");
		TryLookUp = Input.IsActionPressed("Up");
		TryCrouch = Input.IsActionPressed("Down");
	}

	public virtual void Update(float delta)
	{
		GetInput();
		Velocity = new Vector2(Velocity.x, Velocity.y + Player.Gravity * delta);
 		Vector2 snap = new Vector2();
		if (Player.IsOnFloor())
		{
			snap = Vector2.Down * 16;
		}
		else
		{
			snap = Vector2.Zero;
		}
		Velocity = Player.MoveAndSlideWithSnap(Velocity, snap, Vector2.Up);

		// Velocity = Player.MoveAndSlide(Velocity, Vector2.Up);

		int slideCount = Player.GetSlideCount();
		if (slideCount > 0)
		{
			for (int i = 0; i < slideCount; i++)
			{
				if (Player.GetSlideCollision(i).Collider is Enemy enemy && Player.GetNode<Timer>("CollisionDamageTimer").TimeLeft == 0)
				{
					if (enemy.GetState() != Enemy.State.Dying)
					{
						Player.GetNode<Timer>("CollisionDamageTimer").Start();
						Player.SubtractFromHealth((float) Math.Floor(Player.MaximumHealth/5f));
					}
				}
			}
		}

		DoGravity(delta);


	// 	if get_floor_velocity().y < 0:
	// position.y += get_floor_velocity().y * get_physics_process_delta_time() - gravity * get_physics_process_delta_time() - 1


	}


	// var slide_count = get_slide_count()
	// if slide_count:
	//     var collision = get_slide_collision(slide_count - 1)
	//     var collider = collision.collider
	public void DoHorizontalMovement()
	{
		Vector2 dir = new Vector2();
		if (TryLeft)
		{
			dir = new Vector2(-1, 0);
			FlipSprites(true);
			Player.GetNode<Position2D>("AttackNozzle").Position = new Vector2(-100, Player.GetNode<Position2D>("AttackNozzle").Position.y); // need to adjust hite for crouch
		}
		if (TryRight)
		{
			dir = new Vector2(1, 0);
			FlipSprites(false);
			Player.GetNode<Position2D>("AttackNozzle").Position = new Vector2(100, Player.GetNode<Position2D>("AttackNozzle").Position.y);
		}

		float speed = (Player.Speed + (Player.BonusSpeed*25));
		float velocityX = 0f;
		if (dir.x != 0)
		{
			velocityX = Mathf.Lerp(Velocity.x, dir.x * speed, _acceleration);
		}
		else
		{
			velocityX = Mathf.Lerp(Velocity.x, 0, _friction);
		}
		Velocity = new Vector2(velocityX, Velocity.y);
	}

	public void FlipSprites(bool flip)
	{
		foreach (Node n in Player.GetNode("Sprites/CntSprites").GetChildren())
		{
			if (n is Sprite sprite)
			{
				sprite.FlipH = flip;
			}
		}
	}

	public void ShowIndividualSprite(string spriteName) // Sprites/SpriteStandDagger StandNoDagger CrouchDagger CrouchNoDagger
	{
		foreach (Node n in Player.GetNode("Sprites/CntSprites").GetChildren())
		{
			if (n is Sprite sprite)
			{
				sprite.Visible = false;
			}
		}
		Player.GetNode<Sprite>("Sprites/CntSprites/" + spriteName).Visible = true;

		// Player.CurrentSpritePath = "Sprites/" + spriteName;

	}

	public void DoGravity(float delta)
	{
		Velocity = new Vector2(Velocity.x, Math.Min(Velocity.y+Player.Gravity*delta, Player.MaxFallSpeed));

		// If we are on the floor, we need to stop accumulate positive y velocity due to gravity
		if (Player.IsOnFloor() && Velocity.y >= 5)
		{
			Velocity = new Vector2(Velocity.x, 5);
		}

		if (!Player.IsOnFloor() && Velocity.y > 5 && Player.CurrentPlayerAction != Player.PlayerAction.Falling && Player.CurrentPlayerAction != Player.PlayerAction.Jumping)
		{
			// if (Player.CurrentPlayerAction == Player.PlayerAction.Moving)
			// {
			// 	GD.Print("moving to falling");
			// }
			Player.SetActionState(Player.PlayerAction.Falling);
		}
	}

	public virtual void Exit()
	{
		
	}

	public void DoAttack()
	{
		if (!TryAttack || Player.GetNode<Timer>("AttackCooldown").TimeLeft > 0)
		{
			return;
		}
		if (Player.HasWeapon)
		{
			Player.PlayAttackAnim("Attack");
		}
		else
		{
			Player.GetNode<AudioData>("Sounds/NoWeapon").StartPlaying = true;
		}
		Player.GetNode<Timer>("AttackCooldown").Start();
		Player.OnTryAttack();
	}
}
