using Godot;
using System;

public class HurtActionState : ActionState
{
	public HurtActionState()
	{
		GD.Print("Error, using empty constructor");
	}

	public HurtActionState(Player player)
	{
		this.Player = player;
		
		// do damage calculations outside of this script. if health is 0 it wouldnt go to this state; it would go to Dying.
		PlayAnimThenLeaveState();
	}

	public async void PlayAnimThenLeaveState()
	{
		Player.PlayHurtAnim("Hurt");
		await ToSignal(Player.GetNode<AnimationPlayer>("AnimHurt"), "animation_finished");
		if (Player.PreviousPlayerAction == Player.PlayerAction.Crouching)
		{
			Player.SetActionState(Player.PlayerAction.Crouching);
		}
		else
		{
			Player.SetActionState(Player.PlayerAction.Idle);
		}

	}

	public override void Update(float delta)
	{
		base.Update(delta);
		
	}
}
