// I am actually not using this at the moment because the attack occurs in multiple states...

using Godot;
using System;

public class AttackingActionState : ActionState
{
	public AttackingActionState()
	{
		GD.Print("Error, using empty constructor");
	}

	public AttackingActionState(Player player)
	{
		this.Player = player;
		Player.OnTryAttack();
		// Do Attack animation (will need to make it a priority in the DoAnim)

	}

	public override void Update(float delta)
	{
		base.Update(delta);
		
	}
}
