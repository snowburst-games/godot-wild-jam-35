using Godot;
using System;

public class DyingActionState : ActionState
{
	public DyingActionState()
	{
		GD.Print("Error, using empty constructor");
	}

	public DyingActionState(Player player)
	{
		this.Player = player;
		this.Player.Dying = true;
		// do damage calculations outside of this script. if health is 0 it wouldnt go to this state; it would go to Dying.
		PlayAnimThenLeaveState();
	}

	public async void PlayAnimThenLeaveState()
	{
		Player.PlayHurtAnim("Die");
		await ToSignal(Player.GetNode<AnimationPlayer>("AnimHurt"), "animation_finished");
		Player.OnDyingCompleted();
	}

	public override void Update(float delta)
	{
		base.Update(delta);
		
	}
}
