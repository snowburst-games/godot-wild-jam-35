using Godot;
using System;

public class JumpingActionState : ActionState
{
	public JumpingActionState()
	{
		GD.Print("Error, using empty constructor");
	}

	public JumpingActionState(Player player)
	{
		this.Player = player;
		if (Player.GetFloorVelocity().y < 0)
		{
			float yToAdd = Player.GetFloorVelocity().y * Player.GetPhysicsProcessDeltaTime() - Player.Gravity * Player.GetPhysicsProcessDeltaTime() - 1;
			Player.Position = new Vector2(Player.Position.x, Player.Position.y + yToAdd);
		}
		Velocity = new Vector2(Velocity.x, -Player.JumpForce);
		Player.PlayActionAnim("Jump");
		// Player.WasOnFloor = false;
	}

	public override void Update(float delta)
	{
		base.Update(delta);

		if (!Input.IsActionPressed("Jump"))
		{
			if (Velocity.y < -Player.MinJumpForce)
			{
				Velocity = new Vector2(Velocity.x, -Player.MinJumpForce);
			}
		}

		if (Player.IsOnCeiling())
		{
			Velocity = new Vector2(Velocity.x, 5);
		}

		DoHorizontalMovement();
		
		DoAttack();
		if (Velocity.y >= 5)
		{
			Player.SetActionState(Player.PlayerAction.Falling);
		}
		
	}
}
