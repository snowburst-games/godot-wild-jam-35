using Godot;
using System;

public class IdleActionState : ActionState
{
	public IdleActionState()
	{
		GD.Print("Error, using empty constructor");
	}

	public IdleActionState(Player player)
	{
		this.Player = player;
		Player.GetNode<Position2D>("AttackNozzle").Position = new Vector2(Player.GetNode<Position2D>("AttackNozzle").Position.x,
			-20);
		
		ShowIndividualSprite(Player.HasWeapon ? "SpriteStandDagger" : "SpriteStandNoDagger");
		// Player.WasOnFloor = true;
	}

	public override void Update(float delta)
	{
		base.Update(delta);
		Player.PlayActionAnim("Idle");
		if (Player.GetNode<Camera2D>("Camera2D").Offset.y == 200)
		{
			Player.GetNode<AnimationPlayer>("AnimCamera").Play("CrouchToIdle");
		}
		if (Player.GetNode<Camera2D>("Camera2D").Offset.y == -200)
		{
			Player.GetNode<AnimationPlayer>("AnimCamera").Play("LookUpToIdle");
		}
		if (TryLeft || TryRight)
		{
			Player.SetActionState(Player.PlayerAction.Moving);
		}
		if (TryJumpStart || Player.JumpBufferTimer.TimeLeft > 0)
		{
			Player.SetActionState(Player.PlayerAction.Jumping);
			Player.JumpBufferTimer.Stop();
		}
		if (TryLookUp)
		{
			Player.SetActionState(Player.PlayerAction.LookingUp);
		}
		if (TryCrouch)
		{
			Player.SetActionState(Player.PlayerAction.Crouching);
		}

		DoAttack();
		// if (TryAttack)
		// {
		// 	Player.SetActionState(Player.PlayerAction.Attacking);
		// 	// OR dont do this if we want to be able to attack while moving? Experiment
		// }
		
	}
}
