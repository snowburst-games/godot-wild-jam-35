using Godot;
using System;

public class CrouchingActionState : ActionState
{
	public CrouchingActionState()
	{
		GD.Print("Error, using empty constructor");
	}

	public CrouchingActionState(Player player)
	{
		this.Player = player;
		Player.PlayActionAnim("Crouch");
		
		Player.GetNode<Position2D>("AttackNozzle").Position = new Vector2(Player.GetNode<Position2D>("AttackNozzle").Position.x,
			30); // TBC

		ShowIndividualSprite(Player.HasWeapon ? "SpriteCrouchDagger" : "SpriteCrouchNoDagger");

	}

	public override void Update(float delta)
	{
		base.Update(delta);


		if (Player.GetNode<Camera2D>("Camera2D").Offset.y == 0)
		{
			Player.GetNode<AnimationPlayer>("AnimCamera").Play("IdleToCrouch");
		}
		if (!TryCrouch)
		{
			Player.SetActionState(Player.PlayerAction.Idle);
		}
		DoAttack();
		
	}

	public override void Exit()
	{
		base.Exit();
		// Player.GetNode<AnimationPlayer>("AnimCamera").Play("CrouchToIdle");
	}
}
