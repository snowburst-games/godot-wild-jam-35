using Godot;
using System;

public class LookingUpActionState : ActionState
{
	public LookingUpActionState()
	{
		GD.Print("Error, using empty constructor");
	}

	public LookingUpActionState(Player player)
	{
		this.Player = player;
		// Do Attack animation (will need to make it a priority in the DoAnim)

	}

	public override void Update(float delta)
	{
		base.Update(delta);


		if (Player.GetNode<Camera2D>("Camera2D").Offset.y == 0)
		{
			Player.GetNode<AnimationPlayer>("AnimCamera").Play("IdleToLookUp");
		}

		if (!TryLookUp)
		{
			Player.SetActionState(Player.PlayerAction.Idle);
		}		
	}
}
