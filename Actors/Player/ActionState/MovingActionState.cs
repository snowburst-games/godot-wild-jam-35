using Godot;
using System;

public class MovingActionState : ActionState
{
	public MovingActionState()
	{
		GD.Print("Error, using empty constructor");
	}

	public MovingActionState(Player player)
	{
		this.Player = player;
		// Player.WasOnFloor = true;
	}

	public override void Update(float delta)
	{
		base.Update(delta);
		DoHorizontalMovement();
		Player.PlayActionAnim(Player.GetNode<Sprite>("Sprites/CntSprites/SpriteStandNoDagger").FlipH ? "WalkLeft" : "WalkRight");

		if (! Player.GetNode<AudioData>("Sounds/Walk").Playing())
		{
			Player.GetNode<AudioData>("Sounds/Walk").StartPlaying = true;
		}

		DoAttack();
		if (TryJumpStart)
		{
			Player.SetActionState(Player.PlayerAction.Jumping);
		}
		if (! TryLeft && !TryRight && !TryJumpStart)
		{
			Player.SetActionState(Player.PlayerAction.Idle);
		}
		
	}

	public override void Exit()
	{
		base.Exit();
		// if (! Player.GetNode<AudioData>("Sounds/Walk").Playing())
		// {
		Player.GetNode<AudioData>("Sounds/Walk").StopLastSoundPlayer();
		// }
	}
}
