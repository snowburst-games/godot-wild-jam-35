using Godot;
using System;
using System.Collections.Generic;

public class Player : KinematicBody2D
{

	[Signal]
	public delegate void PlayerStatsChanged(float health, float maxHealth, float bonusDmg, float bonusSpeed);

	private float _maximumHealth = 10f;
	private float _bonusDamage = 0f;
	private float _bonusSpeed = 0f;

	// public string CurrentSpritePath = "Sprites/SpriteStandNoDagger";

	public float CurrentHealth {get; set;} = 10f;
	public float MaximumHealth {
		get {
			return _maximumHealth;
		}
		set{
			_maximumHealth = value;
			EmitSignal(nameof(Player.PlayerStatsChanged), CurrentHealth, _maximumHealth, BonusDamage, BonusSpeed);
		}
	}

	public float BonusDamage {
		get {
			return _bonusDamage;
		}
		set{
			_bonusDamage = value;
			EmitSignal(nameof(Player.PlayerStatsChanged), CurrentHealth, _maximumHealth, BonusDamage, BonusSpeed);
		}
	}
	public float BonusSpeed {
		get {
			return _bonusSpeed;
		}
		set{
			_bonusSpeed = Mathf.Clamp(value, 0, 20);
			EmitSignal(nameof(Player.PlayerStatsChanged), CurrentHealth, _maximumHealth, BonusDamage, BonusSpeed);
		}
	}

	public bool HasWeapon = false;
	public bool Dying {get; set;} = false;

	public enum PlayerAction { Idle, Attacking, Dying, Falling, Hurt, Jumping, Moving, Crouching, LookingUp}

	private ActionState _currentActionState;
	public PlayerAction CurrentPlayerAction {get; set;}
	public PlayerAction PreviousPlayerAction {get; set;}

	public Timer CoyoteTimer {get; set;}
	public Timer JumpBufferTimer {get; set;}

	public float Speed {get; set;} = 1000f;// affected by delta
	public float Gravity {get; set;} = 2500f; // affected by delta
	public float JumpForce {get; set;} = 1500f; // affected by delta
	public float MinJumpForce {get; set;} = 500f;
	public float MaxFallSpeed {get; set;} = 4000f;

	public bool HasFinalKey {get; set;} = false;

	public List<string> Keys = new List<string>();

	// public bool WasOnFloor {get; set;} = false;
	
	public override void _Ready()
	{
		CoyoteTimer = GetNode<Timer>("CoyoteTimer");
		JumpBufferTimer = GetNode<Timer>("JumpBufferTimer");
		SetActionState(PlayerAction.Idle);

		// CurrentHealth = MaximumHealth;

	}

	public void SetActionState(PlayerAction state)
	{
		PreviousPlayerAction = CurrentPlayerAction;
		CurrentPlayerAction = state;
		GetNode<Label>("StateLabel").Text = CurrentPlayerAction.ToString();

		if (_currentActionState != null)
		{
			_currentActionState.Exit();
		}
		GetNode<AudioData>("Sounds/Walk").StopLastSoundPlayer();

		switch (state)
		{
			case PlayerAction.Idle:
				_currentActionState = new IdleActionState(this);
				break;
			case PlayerAction.Attacking:
				_currentActionState = new AttackingActionState(this);
				break;
			case PlayerAction.Dying:
				_currentActionState = new DyingActionState(this);
				break;
			case PlayerAction.Falling:
				_currentActionState = new FallingActionState(this);
				break;
			case PlayerAction.Hurt:
				_currentActionState = new HurtActionState(this);
				break;
			case PlayerAction.Jumping:
				_currentActionState = new JumpingActionState(this);
				break;
			case PlayerAction.Moving:
				_currentActionState = new MovingActionState(this);
				break;
			case PlayerAction.Crouching:
				_currentActionState = new CrouchingActionState(this);
				break;
			case PlayerAction.LookingUp:
				_currentActionState = new LookingUpActionState(this);
				break;
		}
	}

	public void StartSpawnTimer()
	{
		// Run after ready
		GetNode<Timer>("SpawnTimer").Start();
		
	}

	public void RefreshStatsDisplay()
	{
		EmitSignal(nameof(Player.PlayerStatsChanged), CurrentHealth, _maximumHealth, BonusDamage, BonusSpeed);
	}

	public override void _PhysicsProcess(float delta)
	{
		base._PhysicsProcess(delta);
		if (Dying)
		{
			return;
		}
		_currentActionState.Update(delta);
	}

	[Signal]
	public delegate void Attacking(Vector2 origin, float direction, float bonusDamage, bool hasWeapon);

	public void OnTryAttack()
	{
		EmitSignal(nameof(Attacking), GetNode<Position2D>("AttackNozzle").GlobalPosition, GetNode<Sprite>("Sprites/CntSprites/SpriteStandNoDagger").FlipH ? 270 : 90, BonusDamage, HasWeapon);
	}

	public void PlayActionAnim(string name)
	{
		if (GetNode<AnimationPlayer>("AnimAction").CurrentAnimation == name	&& GetNode<AnimationPlayer>("AnimAction").IsPlaying())
		{
			return;
		}
		GetNode<AnimationPlayer>("AnimAction").Play(name);
	}

	public void PlayHurtAnim(string name)
	{
		if (GetNode<AnimationPlayer>("AnimHurt").CurrentAnimation == name	&& GetNode<AnimationPlayer>("AnimHurt").IsPlaying())
		{
			return;
		}
		GetNode<AnimationPlayer>("AnimHurt").Play(name);
	}

	public void PlayAttackAnim(string name)
	{
		if (GetNode<AnimationPlayer>("AnimAttack").CurrentAnimation == name	&& GetNode<AnimationPlayer>("AnimAttack").IsPlaying())
		{
			return;
		}
		GetNode<AnimationPlayer>("AnimAttack").Play(name);
	}

	[Signal]
	public delegate void FinishedDying();

	public void OnDyingCompleted()
	{
		EmitSignal(nameof(FinishedDying));
	}

	public void ActivateWeapon(bool activate)
	{
		HasWeapon = activate;
		_currentActionState.ShowIndividualSprite(CurrentPlayerAction == PlayerAction.Crouching ? "SpriteCrouchDagger" : "SpriteStandDagger");
	}

	public void SubtractFromHealth(float dmg)
	{
		if (CurrentPlayerAction == PlayerAction.Dying)
		{
			return;
		}
		CurrentHealth = Mathf.Clamp(CurrentHealth - dmg, 0, MaximumHealth);
		EmitSignal(nameof(Player.PlayerStatsChanged), CurrentHealth, _maximumHealth, BonusDamage, BonusSpeed);
		if (dmg < 0)
		{
			// This is healing. Can play some kind of healing animation if we make one.
			return;
		}
		if (CurrentHealth > 0)
		{
			SetActionState(PlayerAction.Hurt);
		}
		else
		{
			SetActionState(PlayerAction.Dying);
		}
	}

	public void ModMaxHealth(float amount)
	{
		MaximumHealth += amount;
		CurrentHealth += amount;
		EmitSignal(nameof(Player.PlayerStatsChanged), CurrentHealth, _maximumHealth, BonusDamage, BonusSpeed);
	}

}
