using Godot;
using System;

public class BossShoot : KinematicBody2D
{
	Vector2 Velocity = new Vector2(600, 1200);
	private float dmg = 5f; // change depending on how strong the spit is
	Timer ExplodeTimer;
	[Export] int speed = 5;

	// Called when the node enters the scene tree for the first time.
	public override void _Ready()
	{
		ExplodeTimer = GetNode<Timer>("ExplodeTimer");
		ExplodeTimer.Start();
	}

	public override void _PhysicsProcess(float delta)
	{
		var CollisionInfo = MoveAndCollide(Transform.x * speed * Velocity* delta);
		if (CollisionInfo != null)
		{
			if (CollisionInfo.Collider is Enemy enemy || CollisionInfo.Collider is BossShoot bossShoot)
			{
				return;
			}
			Velocity = Velocity.Bounce(CollisionInfo.Normal);
			if (CollisionInfo.Collider is Player player)
			{
				player.SubtractFromHealth(dmg);
				QueueFree();
			}

		}
	}

	public void OnExplodeTimerTimeout()
	{
		QueueFree();
	}



}
