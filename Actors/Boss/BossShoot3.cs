using Godot;
using System;

public class BossShoot3 : Area2D
{
    [Export] int _speed = 800;
	Vector2 _target;
	public Vector2 Velocity = new Vector2(1,2);
	Timer _timer;
    private float dmg = 1f; // change depending on how strong the spit is
    Timer ExplodeTimer;
    public override void _Ready()
    {
        ExplodeTimer = GetNode<Timer>("ExplodeTimer");
		ExplodeTimer.Start();
        
    }

    public override void _PhysicsProcess(float delta)
	{
		Position += Transform.x*Velocity*_speed * delta;      
       // Position += Transform.y *_speed * delta;      

	}


    public void OnBossShootBodyEntered(Node body)
	{
	   if (body is TileMap TileMap|| body is StaticBody2D )
	   {
           Velocity.y= -Velocity.y;
           return;
           
	   }

	   if (body is Player player)
	   {
		   player.SubtractFromHealth(dmg);
           QueueFree();
	   }

	   // It should die on entering any non enemy body
	  if (body is Enemy enemy)
	   {
		   return;
	   }
	   
		
	}


   public void OnExplodeTimerTimeout()
	{
		QueueFree();
	}
}
