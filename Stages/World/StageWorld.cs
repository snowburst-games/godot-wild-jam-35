using Godot;
using System;
using System.Collections.Generic;

public class StageWorld : Stage
{
	private bool _changingLevel = false;

	private Dictionary<Level.WorldLevel, PackedScene> _levelScns = new Dictionary<Level.WorldLevel, PackedScene>()
	{
		{Level.WorldLevel.Start, GD.Load<PackedScene>("res://Levels/Level1/Level1.tscn")},
		{Level.WorldLevel.ForbiddenForest, GD.Load<PackedScene>("res://Levels/Level2/Level2.tscn")},
		{Level.WorldLevel.Underworld, GD.Load<PackedScene>("res://Levels/Level3/Level3.tscn")},
		{Level.WorldLevel.NemesisLair, GD.Load<PackedScene>("res://Levels/Level4/Level4.tscn")},
		{Level.WorldLevel.Final, GD.Load<PackedScene>("res://Levels/Level5/Level5.tscn")}
	};

	private Level.WorldLevel _currentLevel = Level.WorldLevel.Start;
	public override void _Ready()
	{
		base._Ready();

		if (SharedData != null)
		{
			
			if (SharedData.ContainsKey("LoadData"))
			{
				if ((bool)SharedData["LoadData"] == true)
				{
					InitialiseWorldFromData(GetWorldData());
					return;
				}
			}
		}


		// These should be initialised whenever the level is loaded
		// ConnectHintsToJournals();
		// ConnectEntityDoorSignals();
		// InitialiseCheckpoints();
		// ConnectPlayerStatsSignal();
		InitEntityManager();
		GetNode<Player>("Level/EntityManager/Player").RefreshStatsDisplay();

		
		GetNode<Label>("HUD/LblNewLevel").Text = GetNode<Level>("Level").LevelIntroText;
		GetNode<AnimationPlayer>("HUD/LblNewLevel/Anim").Play("Start");

	}

	public void Rebirth()
	{
		// if (SceneManager.GetNode<AnimationPlayer>("Anim").IsPlaying())
		// {
		// 	await ToSignal(SceneManager.GetNode<AnimationPlayer>("Anim"), "animation_finished");
		// }
		GetNode<AnimationPlayer>("HUD/LblRebirth/Anim").Play("Rebirth");
	}

	public void ConnectEntityManagerSignals()
	{
		GetNode<EntityManager>("Level/EntityManager").Connect(nameof(EntityManager.EnteringNewLevel), this, nameof(OnEnteringNewLevel));
		GetNode<EntityManager>("Level/EntityManager").Connect(nameof(EntityManager.NewStoryEvent), this, nameof(OnNewStoryEvent));
		GetNode<EntityManager>("Level/EntityManager").Connect(nameof(EntityManager.NewJournalEntry), this, nameof(OnNewJournalEntry));

	}

	public void OnNewJournalEntry(string text)
	{
		GetNode<PnlJournal>("HUD/PnlJournal").AddTextToJournal(text);
		
		if (GetNode<AnimationPlayer>("HUD/PnlAddedToJournal/Anim").IsPlaying())
		{
			GetNode<AnimationPlayer>("HUD/PnlAddedToJournal/Anim").Stop();
		}
		GetNode<AnimationPlayer>("HUD/PnlAddedToJournal/Anim").Play("JournalUpdated");
	}

	public void OnNewStoryEvent(string text)
	{
		GetNode<HUD>("HUD").PlayGlobalStoryText(text);
	}

	public void OnEnteringNewLevel(Level.WorldLevel destination)
	{
		if (_changingLevel)
		{
			return;
		}

		// if (destination == Level.WorldLevel.Final)
		// {
		// 	if (!GetNode<Player>("Level/EntityManager/Player").HasFinalKey)
		// 	{
		// 		OnNewStoryEvent("YOU DO NOT POSSESS THE KEY. PERHAPS HELD BY A FOUL DENIZEN?");
		// 		return;
		// 	}
		// }

		// save before entering the new level
		OnCheckpointActivated();
		Player player = GetNode<Player>("Level/EntityManager/Player");
		PlayerData playerData = new PlayerData()
		{
			HasWeapon = player.HasWeapon,
			BonusSpeed = player.BonusSpeed,
			BonusDamage = player.BonusDamage,
			CurrentHealth = player.CurrentHealth,
			MaxHealth = player.MaximumHealth,
			HasFinalKey = player.HasFinalKey,
			PlayerKeys = player.Keys
		};

		InitialiseLevelFromData(GetLevelData(destination), destination, playerData);
		// InitPlayerData(GetNode<Player>("Level/EntityManager/Player"), playerData);
	}

	public void InitialiseHints()
	{
		foreach (Node n in GetNode("Level/EntityManager/Hints").GetChildren())
		{
			if (n is Hint hint)
			{
				hint.Connect(nameof(Hint.JournalHintFound), this, nameof(OnNewJournalEntry));
				hint.GetNode<AudioData>("NarrationAudioData").SoundParent = GetPath();
			}
		}
		foreach (Node n in GetNode("Level/EntityManager/Keys").GetChildren())
		{
			if (n is Key key)
			{
				key.Connect(nameof(Key.JournalHintFound), this, nameof(OnNewJournalEntry));
			}
		}
	}

	private void OnBtnExitPressed()
	{
		SceneManager.SimpleChangeScene(SceneData.Stage.MainMenu);
	}

	private void ConnectPlayerSignals()
	{
		GetNode<Player>("Level/EntityManager/Player").Connect(nameof(Player.PlayerStatsChanged), GetNode<HUD>("HUD"), nameof(HUD.OnPlayerStatsChange));
		GetNode<Player>("Level/EntityManager/Player").Connect(nameof(Player.FinishedDying), this, nameof(OnPlayerDyingCompleted));
	}

	private void OnPlayerDyingCompleted()
	{
		// load from last checkpoint
		SceneManager.SimpleChangeScene(SceneData.Stage.World, new Dictionary<string, object>() {{"LoadData", true}, {"Died", true}});
		// InitialiseWorldFromData(GetWorldData()); // this should work but it crashes for some reason
	}

	private void InitialiseCheckpoints()
	{
		foreach (Node n in GetNode("Level/EntityManager/Checkpoints").GetChildren())
		{
			if (n is Checkpoint checkpoint)
			{
				checkpoint.Connect(nameof(Checkpoint.Activated), this, nameof(OnCheckpointActivated));
			}
		}
	}

	private void OnCheckpointActivated()
	{
		if (GetNode<AnimationPlayer>("HUD/PnlSavingProgress/Anim").IsPlaying())
		{
			GetNode<AnimationPlayer>("HUD/PnlSavingProgress/Anim").Stop();
		}
		GetNode<AnimationPlayer>("HUD/PnlSavingProgress/Anim").Play("Saving");
		SaveWorldData();
		SaveLevelData();
	}


	private void SaveWorldData()
	{
		WorldCheckpointData worldCheckpointData = new WorldCheckpointData();
		PnlJournal journal = GetNode<PnlJournal>("HUD/PnlJournal");
		Player player = GetNode<Player>("Level/EntityManager/Player");
		worldCheckpointData.StoreJournal(journal.Pages, journal.AllCurrentTexts, journal.CurrentPage);
		worldCheckpointData.StorePlayerData(player.HasWeapon, player.BonusSpeed, player.BonusDamage, player.CurrentHealth, player.MaximumHealth, player.HasFinalKey,
			player.Keys);
		worldCheckpointData.StoreLevelData(_currentLevel);

		FileJSON.SaveToJSON(OS.GetUserDataDir() + "/WorldData.json", worldCheckpointData);
	}

	private void SaveLevelData()
	{
		LevelCheckpointData levelCheckPointData = new LevelCheckpointData();

		List<Tuple<Vector2, bool>> enemyData = new List<Tuple<Vector2, bool>>();
		foreach (Node n in GetNode("Level/EntityManager/Enemies").GetChildren())
		{
			if (n is Enemy enemy)
			{
				enemyData.Add(new Tuple<Vector2, bool>(item1:enemy.StartPosition, item2: enemy.Boss));
			}
		}
		// List<Tuple<Tuple<Vector2, string>, Tuple<string,bool>>> hintData = new List<Tuple<Tuple<Vector2, string>, Tuple<string,bool>>>();
		List<Dictionary<string, object>> hintData = new List<Dictionary<string, object>>();
		foreach (Node n in GetNode("Level/EntityManager/Hints").GetChildren())
		{
			if (n is Hint hint)
			{
				Dictionary<string, object> newHintData = new Dictionary<string, object>();
				newHintData["PosX"] = hint.Position.x;
				newHintData["PosY"] = hint.Position.y;
				newHintData["AudioPath"] = hint.AudioPath;
				newHintData["HintText"] = hint.HintText;
				newHintData["AddToJournal"] = hint.AddToJournal;
				newHintData["Instructions"] = hint.Instructions;
				newHintData["SpritePath"] = hint.SpritePath;
				hintData.Add(newHintData);
				// Tuple<Vector2, string> hintPosAudio = new Tuple<Vector2, string>(item1:hint.Position, item2:hint.AudioPath);
				// Tuple<string,bool> hintTextJournal = new Tuple<string, bool>(item1:hint.HintText, item2:hint.AddToJournal);
				// hintData.Add(new Tuple<Tuple<Vector2, string>, Tuple<string,bool>>(item1:hintPosAudio, item2: hintTextJournal));
			}
		}
		List<Tuple<Vector2, PowerUp.PowerUpType>> powerUpData = new List<Tuple<Vector2, PowerUp.PowerUpType>>();
		foreach (Node n in GetNode("Level/EntityManager/PowerUps").GetChildren())
		{
			if (n is PowerUp powerUp)
			{
				powerUpData.Add(new Tuple<Vector2, PowerUp.PowerUpType>(item1:powerUp.Position, item2: powerUp.CurrentPowerUpType));
			}
		}
		List<Tuple<Vector2, string>> keyData = new List<Tuple<Vector2, string>>();
		foreach (Node n in GetNode("Level/EntityManager/Keys").GetChildren())
		{
			if (n is Key key)
			{
				keyData.Add(new Tuple<Vector2, string>(item1:key.Position, item2: key.KeyText));
			}
		}
		

		levelCheckPointData.StoreEnemyData(enemyData);
		levelCheckPointData.StoreHints(hintData);
		levelCheckPointData.StoreKeys(keyData);
		levelCheckPointData.StorePowerUps(powerUpData);
		levelCheckPointData.StoreLevelData(GetNode<Level>("Level").CurrentWorldLevel);
		levelCheckPointData.PlayerPosition = GetNode<Player>("Level/EntityManager/Player").Position;

		FileJSON.SaveToJSON(OS.GetUserDataDir() + "/LevelData" + GetNode<Level>("Level").CurrentWorldLevel + ".json", levelCheckPointData);
	}

	// public override void _Input(InputEvent ev)
	// {
	// 	if (ev.IsActionPressed("ui_accept"))
	// 	{
	// 		InitialiseWorldFromData(GetWorldData());	
	// 	}
	// }

	private WorldCheckpointData GetWorldData()
	{
		string path = OS.GetUserDataDir() + "/WorldData.json";
		if (! System.IO.File.Exists(path))
		{
			GD.Print("File at " + path + " not found.");
			return null;
		}
		WorldCheckpointData data = FileJSON.LoadFromJSON<WorldCheckpointData>(path);
		return data;
	}
	private LevelCheckpointData GetLevelData(Level.WorldLevel level)
	{
		string path = OS.GetUserDataDir() + "/LevelData" + level + ".json";
		if (! System.IO.File.Exists(path))
		{
			GD.Print("File at " + path + " not found.");
			return null;
		}
		LevelCheckpointData data = FileJSON.LoadFromJSON<LevelCheckpointData>(path);
		return data;
	}

	private void InitialiseWorldFromData(WorldCheckpointData data)
	{
		if (_changingLevel)
		{
			return;
		}
		if (HasNode("Level"))
		{
			GetNode("Level").Free();
		}
		if (data == null)
		{
			InitialiseLevelFromData(null, Level.WorldLevel.Start, null);
		}
		else
		{
			InitialiseLevelFromData(GetLevelData(data.CurrentLevel), data.CurrentLevel, new PlayerData {
				HasWeapon = data.PlayerHasWeapon,
				BonusSpeed = data.PlayerBonusSpeed,
				BonusDamage = data.PlayerBonusDamage,
				CurrentHealth = data.PlayerCurrentHealth,
				MaxHealth = data.PlayerMaxHealth,
				HasFinalKey = data.PlayerHasFinalKey,
				PlayerKeys = data.PlayerKeys});
			GetNode<PnlJournal>("HUD/PnlJournal").LoadJournal(data.JournalPages, data.CurrentTextsInJournal, data.CurrentJournalPage);
		}

	}

	public void InitPlayerData(Player player, PlayerData playerData)
	{
		player.ActivateWeapon(playerData.HasWeapon);
		player.BonusSpeed = playerData.BonusSpeed;
		player.BonusDamage =  playerData.BonusDamage;
		player.CurrentHealth =  playerData.CurrentHealth;
		player.MaximumHealth =  playerData.MaxHealth;
		player.HasFinalKey = playerData.HasFinalKey;
		player.Keys = playerData.PlayerKeys;
	}

	private async void InitialiseLevelFromData(LevelCheckpointData data, Level.WorldLevel targetLevel, PlayerData playerData)
	{
		if (_changingLevel)
		{
			return;
		}
		_changingLevel = true;

		bool rebirthing = false;
		if (SharedData != null)
		{
			if (SharedData.ContainsKey("Died"))
			{
				if ((bool)SharedData["Died"] == true)
				{
					rebirthing = true;
				}
			}
		}

		AnimationPlayer blackRectAnim = GetNode<AnimationPlayer>("HUD/BlackRect/Anim");
		if (!rebirthing)
		{
			blackRectAnim.Play("FadeIn");
		
			await ToSignal(blackRectAnim, "animation_finished");
		}
		if (HasNode("Level"))
		{
			GetNode<Level>("Level").Name = "OldLevel";
			GetNode<Level>("OldLevel").QueueFree();
		}
		Level newLevel;
		if (data == null)
		{
			// just load the level default values
			newLevel = (Level)_levelScns[targetLevel].Instance();
		}
		else
		{
			newLevel = (Level)_levelScns[data.Level].Instance();
		}
			
		AddChild(newLevel);
		newLevel.Name = "Level";
		if (data != null)
		{
			ClearLevelNodes(newLevel);
			PopulateLevelNodes(newLevel, data);
		}
		InitEntityManager();
		_currentLevel = newLevel.CurrentWorldLevel;

		if (playerData != null)
		{
			InitPlayerData(newLevel.GetNode<Player>("EntityManager/Player"), playerData);
			GetNode<Player>("Level/EntityManager/Player").RefreshStatsDisplay();
		}

		GetNode<Player>("Level/EntityManager/Player").RefreshStatsDisplay();
		blackRectAnim.Play("FadeOut");
		await ToSignal(blackRectAnim, "animation_finished");
		_changingLevel = false;

		if (rebirthing)
		{
			Rebirth();
			SharedData["Died"] = false;
			return;
		}

		GetNode<Label>("HUD/LblNewLevel").Text = newLevel.LevelIntroText;
		GetNode<AnimationPlayer>("HUD/LblNewLevel/Anim").Play("Start");
	}

	public void InitEntityManager()
	{
		InitialiseHints();
		InitialiseCheckpoints();
		ConnectEntityManagerSignals();
		ConnectPlayerSignals();
		GetNode<PowerUps>("Level/EntityManager/PowerUps").ConnectPowerUps();
		GetNode<Keys>("Level/EntityManager/Keys").ConnectKeys();
		GetNode<Player>("Level/EntityManager/Player").StartSpawnTimer();
		if (GetNode("Level/EntityManager").HasNode("ElixirOfLife"))
		{
			GetNode<ElixirOfLife>("Level/EntityManager/ElixirOfLife").Connect(nameof(ElixirOfLife.ElixirRetrieved), this, nameof(OnElixirOfLifeRetrieved));
		}
	}

	public void OnElixirOfLifeRetrieved()
	{
		GetNode<HUD>("HUD").OnVictory();
	}

	private void ClearLevelNodes(Level level)
	{
		foreach (Node n in level.GetNode("EntityManager/Enemies").GetChildren())
		{
			if (n is Enemy enemy)
			{
				enemy.Free();
			}
		}
		foreach (Node n in level.GetNode("EntityManager/PowerUps").GetChildren())
		{
			if (n is PowerUp powerUp)
			{
				powerUp.Free();
			}
		}
		foreach (Node n in level.GetNode("EntityManager/Hints").GetChildren())
		{
			if (n is Hint hint)
			{
				hint.Free();
			}
		}
		foreach (Node n in level.GetNode("EntityManager/Keys").GetChildren())
		{
			if (n is Key key)
			{
				key.Free();
			}
		}
	}

	private void PopulateLevelNodes(Level level, LevelCheckpointData data)
	{
		foreach (Tuple<Vector2, bool> enemyData in data.EnemyData)
		{
			PackedScene enemyScn;
			if (enemyData.Item2) // if boss
			{
				enemyScn = GD.Load<PackedScene>("res://Actors/Enemy/Boss.tscn");
			}
			else
			{
				enemyScn = GD.Load<PackedScene>("res://Actors/Enemy/Enemy.tscn");
			}
			Enemy enemy = (Enemy) enemyScn.Instance();
			enemy.Position = enemyData.Item1;
			enemy.Boss = enemyData.Item2; // other data needed? await sarah's reply
			level.GetNode("EntityManager/Enemies").AddChild(enemy);
		}
		// foreach (Tuple<Tuple<Vector2, string>, Tuple<string,bool>> hintData in data.HintData) //List<Tuple<Vector2, Tuple<string,bool>>>
		// {
		// 	Hint hint = (Hint) (GD.Load<PackedScene>("res://Props/Hint/Hint.tscn")).Instance();
		// 	hint.Position = hintData.Item1.Item1;
		// 	hint.AudioPath = hintData.Item1.Item2;
		// 	hint.HintText = hintData.Item2.Item1;
		// 	hint.AddToJournal = hintData.Item2.Item2;
		// 	level.GetNode("EntityManager/Hints").AddChild(hint);
		// }
		foreach (Dictionary<string, object> hintData in data.HintData) //List<Tuple<Vector2, Tuple<string,bool>>>
		{
			Hint hint = (Hint) (GD.Load<PackedScene>("res://Props/Hint/Hint.tscn")).Instance();
			hint.Position = new Vector2( float.Parse(hintData["PosX"].ToString()), float.Parse(hintData["PosY"].ToString()));
			hint.AudioPath = (string) hintData["AudioPath"];
			hint.HintText = (string) hintData["HintText"];
			hint.AddToJournal = (bool) hintData["AddToJournal"];
			hint.Instructions = int.Parse(hintData["Instructions"].ToString());
			hint.SpritePath = (string) hintData["SpritePath"];
			level.GetNode("EntityManager/Hints").AddChild(hint);
		}

				// Dictionary<string, object> newHintData = new Dictionary<string, object>();
				// newHintData["Position"] = hint.Position;
				// newHintData["AudioPath"] = hint.AudioPath;
				// newHintData["HintText"] = hint.HintText;
				// newHintData["AddToJournal"] = hint.AddToJournal;
				// newHintData["Instructions"] = hint.Instructions;
				// newHintData["SpritePath"] = hint.SpritePath;
				// hintData.Add(newHintData);
		foreach (Tuple<Vector2, PowerUp.PowerUpType> powerUpData in data.PowerUpData)
		{
			PackedScene powerUpScene;
			switch (powerUpData.Item2)
			{
				case PowerUp.PowerUpType.Damage:
					powerUpScene = GD.Load<PackedScene>("res://Props/Powerup/PowerUpDamage.tscn");
					break;
				case PowerUp.PowerUpType.Health:
					powerUpScene = GD.Load<PackedScene>("res://Props/Powerup/PowerUpHealth.tscn");
					break;
				case PowerUp.PowerUpType.Speed:
					powerUpScene = GD.Load<PackedScene>("res://Props/Powerup/PowerUpSpeed.tscn");
					break;
				case PowerUp.PowerUpType.Weapon:
					powerUpScene = GD.Load<PackedScene>("res://Props/Powerup/PowerUpWeapon.tscn");
					break;
				case PowerUp.PowerUpType.HealthBig:
					powerUpScene = GD.Load<PackedScene>("res://Props/Powerup/PowerUpHealthBig.tscn");
					break;
				case PowerUp.PowerUpType.MaxHealth:
					powerUpScene = GD.Load<PackedScene>("res://Props/Powerup/PowerUpMaxHealth.tscn");
					break;
				default:
					powerUpScene = GD.Load<PackedScene>("res://Props/Powerup/PowerUpHealth.tscn");
					GD.Print("ERROR in setting powerup!");
					break;
			}
			PowerUp powerUp = (PowerUp) powerUpScene.Instance();
			powerUp.CurrentPowerUpType = powerUpData.Item2;
			powerUp.Position = powerUpData.Item1;
			level.GetNode("EntityManager/PowerUps").AddChild(powerUp);
		}

		foreach (Tuple<Vector2, string> keyData in data.KeyData)
		{
			Key key = (Key) (GD.Load<PackedScene>("res://Props/Keys/Key.tscn")).Instance();
			key.Position = keyData.Item1;
			key.KeyText = keyData.Item2;
			level.GetNode("EntityManager/Keys").AddChild(key);
		}

		level.GetNode<Player>("EntityManager/Player").Position = data.PlayerPosition;
		level.GetNode<Player>("EntityManager/Player").Dying = false;

		level.GetNode<EntityManager>("EntityManager").SetupEnemies();
	}

}
/*

	private List<int> GetLevelsCompleted()
	{
		string path = OS.GetUserDataDir() + "/CompletedLevels.json";
		if (! System.IO.File.Exists(path))
		{
			GD.Print("File at " + path + " not found.");
			return null;
		}
		LevelData data = FileJSON.LoadFromJSON<LevelData>(path);
		return data.LevelsCompleted;
	}
	*/
