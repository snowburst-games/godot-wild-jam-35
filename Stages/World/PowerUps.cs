using Godot;
using System;

public class PowerUps : Node2D
{
	private PackedScene _floatLblScene;

	public void ConnectPowerUps()
	{
		foreach (Node n in GetChildren())
		{
			if (n is PowerUp powerUp)
			{
				powerUp.Connect(nameof(PowerUp.AnnounceActivated), this, nameof(MakeFloatLabel));
			}
		}
	}

	public void MakeFloatLabel(string text, Vector2 location)
	{
		_floatLblScene = GD.Load<PackedScene>("res://Interface/Labels/FloatScoreLabel/LblFloatScore.tscn");
		LblFloatScore lbl = (LblFloatScore) _floatLblScene.Instance();
		lbl.Text = text;
		lbl.RectPosition = location + new Vector2(-135, 0);
		GetNode("PowerUpFloatLabels").AddChild(lbl);
	}
}
