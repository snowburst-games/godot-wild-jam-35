using Godot;
using System;

public class Keys : Node2D
{
	private PackedScene _floatLblScene;
	public void ConnectKeys()
	{
		foreach (Node n in GetChildren())
		{
			if (n is Key key)
			{
				key.Connect(nameof(Key.AnnounceActivated), this, nameof(MakeFloatLabel));
			}
		}
	}

	public void MakeFloatLabel(string text, Vector2 location)
	{
		_floatLblScene = GD.Load<PackedScene>("res://Interface/Labels/FloatScoreLabel/LblFloatScore.tscn");
		LblFloatScore lbl = (LblFloatScore) _floatLblScene.Instance();
		lbl.Text = text;
		lbl.RectPosition = location + new Vector2(-135, 0);
		GetNode("KeyFloatLabels").AddChild(lbl);
	}
}
