using Godot;
using System;
using System.Collections.Generic;

public class PnlJournal : Panel
{

	private float _maxHeight;
	public List<string> Pages {get; set;} = new List<string>() {""}; // initilaise a list with an empty string at 0

	public List<string> AllCurrentTexts {get; set;} = new List<string>(); // to check for duplicates

	public int CurrentPage {get; set;} = 0; // start at page 0

	public override void _Ready()
	{
		Visible = false;
		_maxHeight = GetNode<Label>("LblBody").RectSize.y;
		// ShowLastPage();
	}

	public void LoadJournal(List<string> pages, List<string> allTexts, int currentPage)
	{
		Pages = pages;
		AllCurrentTexts = allTexts;
		SetCurrentPage(currentPage);
	}


	public void SetButtons()
	{
		GetNode<AnimationPlayer>("TurnPageBtns/Anim").Stop();
		GetNode<Button>("TurnPageBtns/BtnLeft").Modulate = new Color(1,1,1,1);
		GetNode<Button>("TurnPageBtns/BtnRight").Modulate = new Color(1,1,1,1);
		GetNode<Button>("TurnPageBtns/BtnLeft").Disabled = false;
		GetNode<Button>("TurnPageBtns/BtnRight").Disabled = false;
		if (CurrentPage == 0)
		{
			GetNode<Button>("TurnPageBtns/BtnLeft").Disabled = true;
		}
		else
		{
			GetNode<AnimationPlayer>("TurnPageBtns/Anim").Play("ActiveLeft");
		}
		if (CurrentPage == Pages.Count-1)
		{
			GetNode<Button>("TurnPageBtns/BtnRight").Disabled = true;
		}
		else
		{
			GetNode<AnimationPlayer>("TurnPageBtns/Anim").Play("ActiveRight");
		}
		if (CurrentPage != 0 && CurrentPage != Pages.Count-1)
		{
			GetNode<AnimationPlayer>("TurnPageBtns/Anim").Play("ActiveBoth");
		}
	}

	private string GetCurrentPageText()
	{
		return Pages[CurrentPage];
	}

	private void SetCurrentPage(int num)
	{
		CurrentPage = num;
		GetNode<Label>("LblPageNumber").Text = (CurrentPage+1).ToString();
		GetNode<Label>("LblBody").Text = GetCurrentPageText();
		SetButtons();
	}

	public void AddTextToJournal(string newText)
	{
		if (AllCurrentTexts.Contains(newText))
		{
			return;
		}
		GetNode<AnimationPlayer>("Anim").Play("ActiveJournalBtn");
		AllCurrentTexts.Add(newText);
		if (LastPageHasEnoughRoom(newText))
		{
			if (Pages[Pages.Count-1] != "")
			{
				Pages[Pages.Count-1] += "\n\n";
			}
			Pages[Pages.Count-1] += newText;
		}
		else
		{
			Pages.Add(newText);
		}
		// Refresh the current page
		SetCurrentPage(CurrentPage);
	}

	private void ShowLastPage()
	{
		SetCurrentPage(Pages.Count-1);
	}

	private bool LastPageHasEnoughRoom(string newText)
	{
		
		Label testBody = (Label) GetNode<Label>("LblBody").Duplicate();
		testBody.Modulate = new Color(1,1,1,0);
		AddChild(testBody);
		testBody.Text = "";
		testBody.Text += Pages[Pages.Count-1];
		testBody.Text += ("\n\n" + newText);
		float height = testBody.GetCombinedMinimumSize().y;
		testBody.QueueFree();
		if (testBody.GetCombinedMinimumSize().y > _maxHeight)
		{
			return false;
		}
		return true;
	}

	private void OnBtnLeftPressed()
	{
		if (CurrentPage > 0)
		{
			SetCurrentPage(CurrentPage - 1);
		}
	}

	private void OnBtnRightPressed()
	{
		if (CurrentPage < Pages.Count-1)
		{
			SetCurrentPage(CurrentPage + 1);
		}
	}

	private void OnBtnClosePressed()
	{
		GetTree().Paused = Visible = false;
	}

	public override void _Input(InputEvent ev)
	{
		base._Input(ev);
		if (ev.IsActionPressed("Journal"))
		{
			GetTree().Paused = Visible = !Visible;
			if (Visible)
			{
				GetNode<AudioData>("AudioData").StartPlaying = true;
			}
		}

	}
	public new bool Visible
	{
		get{
			return base.Visible;
		}
		set{
			base.Visible = value;
			if (value)
			{
				GetNode<AnimationPlayer>("Anim").Play("StopActiveJournalBtn");
			}
		}
	}
}

