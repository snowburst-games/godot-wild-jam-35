using Godot;
using System;

public class HUD : CanvasLayer
{
	public override void _Ready()
	{
		GetNode<PnlSettings>("PnlSettings").Visible = GetNode<Panel>("PnlMenu").Visible = false;

	}
	
	private void OnBtnJournalPressed()
	{
		GetTree().Paused = GetNode<PnlJournal>("PnlJournal").Visible = true;
		GetNode<AudioData>("PnlJournal/AudioData").StartPlaying = true;
	}

	private void OnBtnMenuPressed()
	{
		GetTree().Paused = GetNode<Panel>("PnlMenu").Visible = true;
	}

	private void OnBtnSettingsPressed()
	{
		GetNode<PnlSettings>("PnlSettings").Visible = true;
	}

	private void OnBtnResumePressed()
	{
		GetTree().Paused = GetNode<Panel>("PnlMenu").Visible = false;
	}

	public override void _Input(InputEvent ev)
	{
		base._Input(ev);

		if (ev.IsActionPressed("Pause"))
		{
			if (!GetNode<PnlSettings>("PnlSettings").Visible)
			{
				GetTree().Paused = GetNode<Panel>("PnlMenu").Visible = ! GetNode<Panel>("PnlMenu").Visible;
			}
		}
		if (ev.IsActionPressed("HideUI"))
		{
			GetNode<VBoxContainer>("CntPlayerInfo").Visible = !GetNode<VBoxContainer>("CntPlayerInfo").Visible;
		}
	}

	public void OnPlayerStatsChange(float health, float maxHealth, float bonusDmg, float bonusSpeed)
	{
		GD.Print(health);
		GetNode<ProgressBar>("CntPlayerInfo/BarHealth").Value = health/maxHealth*100f;
		GetNode<Label>("CntPlayerInfo/BarHealth/Label").Text = (health.ToString() + " / " + maxHealth.ToString());

		GetNode<Label>("CntPlayerInfo/LblBonusDmg").Text = "Bonus Damage: " + bonusDmg.ToString();
		GetNode<Label>("CntPlayerInfo/LblBonusSpeed").Text = "Bonus Speed: " + bonusSpeed.ToString();

	}

	public void PlayGlobalStoryText(string text)
	{
		GetNode<Label>("LblStoryHUD").Text = text;
		GetNode<AnimationPlayer>("LblStoryHUD/Anim").Play("ShowText");
	}

	public void OnVictory()
	{
		GetTree().Paused = true;
		GetNode<AnimationPlayer>("PnlVictory/Anim").Play("Victory");
	}

}

