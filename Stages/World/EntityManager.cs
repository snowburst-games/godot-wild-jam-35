using Godot;
using System;

public class EntityManager : Node2D
{

	[Signal]
	public delegate void EnteringNewLevel(Level.WorldLevel level);

	[Signal]
	public delegate void NewStoryEvent(string text);

	[Signal]
	public delegate void NewJournalEntry(string text);

	private PackedScene _knifeScn;
	public override void _Ready()
	{
		_knifeScn = GD.Load<PackedScene>("res://Props/Knife/Knife.tscn");
		GetNode<Player>("Player").Connect(nameof(Player.Attacking), this, nameof(OnPlayerAttacking));

		foreach (Node n in GetNode("Checkpoints").GetChildren())
		{
			if (n is Checkpoint checkpoint)
			{
				CheckpointSetup(checkpoint);
			}
		}
		foreach (Node n in GetNode("Doors").GetChildren())
		{
			if (n is LevelDoor door)
			{
				DoorSetup(door);
			}
		}
		
		// OnEnemyDied(true, GetNode<Player>("Player").Position + new Vector2(300,0));
		SetupEnemies();
	}

	public void SetupEnemies() // need to run after new enemies are loaded
	{
		foreach (Node n in GetNode("Enemies").GetChildren())
		{
			if (n is Enemy enemy)
			{
				EnemySetup(enemy);
			}
		}
	}

	private void OnPlayerAttacking(Vector2 origin, float rotationDegrees, float bonusDamage, bool hasWeapon)
	{
		if (!hasWeapon)
		{
			EmitSignal(nameof(NewStoryEvent), "No weapon equipped! Retrieve your blade!");
			return;
		}
		Knife knife = (Knife) _knifeScn.Instance();
		AddChild(knife);
		knife.Position = origin;
		knife.Damage += bonusDamage;
		knife.Init(rotationDegrees);
	}

	private void OnEnemyAttacking(PackedScene shootScn, Vector2 position, Vector2 lastKnownPlayerPosition)
	{
		Node2D bullet = (Node2D) shootScn.Instance();
		AddChild(bullet);
		bullet.Position = position;
		bullet.LookAt(lastKnownPlayerPosition);
	}
	private void OnBossAttacking(PackedScene shootScn, Vector2 position, Vector2 lastKnownPlayerPosition) //NEW
	{
		Node2D bullet = (Node2D) shootScn.Instance();
		AddChild(bullet);
		bullet.Position = position;
		bullet.LookAt(lastKnownPlayerPosition);
	}
	
	private void EnemySetup(Enemy enemy)
	{
		enemy.Connect(nameof(Enemy.OnLookingForPlayer), this, nameof(GivePlayerPosition));
		enemy.Connect(nameof(Enemy.Attacking), this, nameof(OnEnemyAttacking));
		enemy.Connect(nameof(Enemy.AttackingBoss),this,nameof(OnBossAttacking)); //NEW
		enemy.Connect(nameof(Enemy.Died), this, nameof(OnEnemyDied));
	}
	
	private bool _bossDied = false; // hack because sarah cant fix her enemy dying bug
	public void OnEnemyDied(bool boss, Vector2 position)
	{
		if (boss)
		{
			if (_bossDied)
			{
				return;
			}
			Key key = (Key) (GD.Load<PackedScene>("res://Props/Keys/Key.tscn")).Instance();
			key.KeyText = "Eternal";
			key.Position = position;
			CallDeferred("add_child", key);// AddChild(key);
			_bossDied = true;
			key.Connect(nameof(Key.AnnounceActivated), GetNode<Keys>("Keys"), nameof(Keys.MakeFloatLabel));
			key.Connect(nameof(Key.JournalHintFound), this, nameof(OnEntityJournalEntry));
			// GetNode<Player>("Player").HasFinalKey = true;

			
		}
	}

	public void OnEntityJournalEntry(string text)
	{
		EmitSignal(nameof(NewJournalEntry), text);
	}

	private void DoorSetup(LevelDoor door)
	{
		door.Connect(nameof(LevelDoor.Activated), this, nameof(OnDoorActivated));
	}

	private void OnDoorActivated(Level.WorldLevel levelDestination)
	{
		EmitSignal(nameof(EnteringNewLevel), levelDestination);
	}

	private void CheckpointSetup(Checkpoint checkpoint)
	{
		// checkpoint.Connect(nameof(Checkpoint.Activated), this, nameof(OnCheckpointActivated));
	}

	public void OnCheckpointActivated()
	{

	}

	private void GivePlayerPosition(Enemy enemy)
	{
		enemy.UpdatePlayerPosition(GetNode<Player>("Player").GlobalPosition);
	}
}
