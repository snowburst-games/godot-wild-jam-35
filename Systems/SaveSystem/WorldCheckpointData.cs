using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class WorldCheckpointData : IJSONSaveable
{
	// would need a checkpoint data for each level, and a "global" player checkpoint data
	// then when the game starts load all the checkpoint datas for level specific e.g. enemies, hints, 
	// and then load the global checkpoint data
	// when saving, save level specific data and global checkpoint data
	public Level.WorldLevel CurrentLevel{get; set;}
	public bool PlayerHasWeapon{get; set;}
	public float PlayerBonusSpeed{get; set;}
	public float PlayerBonusDamage{get; set;}
	public float PlayerCurrentHealth{get; set;}
	public float PlayerMaxHealth {get; set;}
	public bool PlayerHasFinalKey {get; set;}

	public List<string> PlayerKeys {get; set;}

	public PlayerData CurrentPlayerData {get; set;} // doesnt work via json

	public List<string> JournalPages {get; set;}
	public List<string> CurrentTextsInJournal {get; set;}
	public int CurrentJournalPage {get; set;}

	public void StoreJournal(List<string> pages, List<string> currentTexts, int currentPage)
	{
		JournalPages = pages.ToList();
		CurrentTextsInJournal = currentTexts.ToList();
		CurrentJournalPage = currentPage;
	}
	
	public void StorePlayerData(bool hasWeapon, float bonusSpeed, float bonusDamage, float currentHealth, float maxHealth, bool hasFinalKey, List<string> playerKeys)
	{
		PlayerHasWeapon = hasWeapon;
		PlayerBonusSpeed = bonusSpeed;
		PlayerBonusDamage = bonusDamage;
		PlayerCurrentHealth = currentHealth;
		PlayerMaxHealth = maxHealth;
		PlayerHasFinalKey = hasFinalKey;
		PlayerKeys = playerKeys;
	}

	public void StoreLevelData(Level.WorldLevel level)
	{
		CurrentLevel = level;
	}

}
