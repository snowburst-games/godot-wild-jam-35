using Godot;
using System;
using System.Collections.Generic;
using System.Linq;

public class LevelCheckpointData : IJSONSaveable
{
	// would need a checkpoint data for each level, and a "global" player checkpoint data
	// then when the game starts load all the checkpoint datas for level specific e.g. enemies, hints, 
	// and then load the global checkpoint data
	// when saving, save level specific data and global checkpoint data
	public Level.WorldLevel Level {get; set;}
	public List<Tuple<Vector2, bool>> EnemyData {get; set;}
	public List<Tuple<Vector2, PowerUp.PowerUpType>> PowerUpData {get; set;}
	// public List<Tuple<Vector2, string>> HintData {get; set;}
	
	// public List<Tuple<Tuple<Tuple<Vector2, string>, Tuple<string,bool>, Tuple<int, string>>> HintData {get; set;}

	public List<Dictionary<string, object>> HintData {get; set;}
	public Vector2 PlayerPosition {get; set;}

	public List<Tuple<Vector2, string>> KeyData {get; set;}

	public void StoreEnemyData(List<Tuple<Vector2, bool>> enemyData)
	{
		EnemyData = enemyData.ToList();
	}

	public void StorePowerUps(List<Tuple<Vector2, PowerUp.PowerUpType>> powerUpData)
	{
		PowerUpData = powerUpData.ToList();
	}

	// public void StoreHints(List<Tuple<Tuple<Vector2, string>, Tuple<string,bool>>> hintData)
	// {
	// 	HintData = hintData.ToList();
	// }
	public void StoreHints(List<Dictionary<string, object>> hintData)
	{
		HintData = hintData.ToList();
	}
	
	public void StoreKeys(List<Tuple<Vector2, string>> keyData)
	{
		KeyData = keyData.ToList();
	}
	
	public void StoreLevelData(Level.WorldLevel level)
	{
		Level = level;
	}
}
