using System;
using System.Collections.Generic;

public class PlayerData
{
    public bool HasWeapon {get; set;}
    public float BonusDamage {get; set;}
    public float BonusSpeed {get; set;}
    public float CurrentHealth {get; set;}
    public float MaxHealth {get; set;}
    public bool HasFinalKey {get; set;}
    public List<string> PlayerKeys {get; set;}
    
}
		// PlayerHasWeapon = hasWeapon;
		// PlayerBonusSpeed = bonusSpeed;
		// PlayerBonusDamage = bonusDamage;
		// PlayerCurrentHealth = currentHealth;
		// PlayerMaxHealth = maxHealth;