using Godot;
using System;

public class Level : Node2D
{
	[Export]
	public WorldLevel CurrentWorldLevel;

	[Export]
	public string LevelIntroText; // this can display when entering the area via the HUD

	public enum WorldLevel { Start, ForbiddenForest, Underworld, NemesisLair, Final}
}
