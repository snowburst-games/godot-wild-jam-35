using Godot;
using System;

public class Checkpoint : Area2D
{
	
	[Signal]
	public delegate void Activated();

	private void OnCheckpointBodyEntered(object body)
	{
		if (body is Player player && GetNode<Timer>("Cooldown").TimeLeft == 0)
		{
			if (GetNode<AnimationPlayer>("Anim").IsPlaying() && GetNode<AnimationPlayer>("Anim").CurrentAnimation == "Spawning")
			{
				return;
			}
			if (player.GetNode<Timer>("SpawnTimer").TimeLeft > 0)
			{
				GetNode<AnimationPlayer>("Anim").Play("Spawning");
				return;
			}
			GetNode<Timer>("Cooldown").Start();
			GetNode<AnimationPlayer>("Anim").Play("Activated");
			EmitSignal(nameof(Activated));

		}
	}

}
