using Godot;
using System;

public class ElixirOfLife : Area2D
{
	[Signal]
	public delegate void ElixirRetrieved();

	private void OnElixirOfLifeBodyEntered(object body)
	{
		if (body is Player player)
		{
			EmitSignal(nameof(ElixirRetrieved));
		}
	}

}

