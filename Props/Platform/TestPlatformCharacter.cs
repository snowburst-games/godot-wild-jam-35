using Godot;
using System;

public class TestPlatformCharacter : KinematicBody2D
{
 [Export] int speed = 1200;
	[Export] int jumpSpeed = -1100;
	[Export] int Gravity = 2000;

	Vector2 velocity = new Vector2();

	Vector2 snap; //FOR PLATFORM

  public override void _Ready()
	{
		
	}

	public void GetInput()
	{
		velocity.x = 0;
		if (Input.IsActionPressed("Right"))
		{
			velocity.x += speed;
		}
		if (Input.IsActionPressed("Left"))
		{
			velocity.x -= speed;
		}
	}

	public override void _PhysicsProcess(float delta)
	{
		GetInput();
		velocity.y += Gravity*delta;
		//FOR PLATFORM: CHANGE TO MOVE AND SLIDE WITH SNAP
		//THEN ADD SNAP VARIABLE (snap value will ensure that the character “sticks” to the platform even if it moves)
		if (IsOnFloor())
		{
			snap = Vector2.Down * 16;
		}
		if (!IsOnFloor())
		{
			snap = Vector2.Zero;
		}
		//var snap = Vector2.Down * 16 
		velocity = MoveAndSlideWithSnap(velocity, snap, Vector2.Up);
		if (Input.IsActionJustPressed("Jump"))
		{
			if (IsOnFloor())
			{
				velocity.y = jumpSpeed;
			}
		}
	}

}
