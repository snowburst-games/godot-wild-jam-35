using Godot;
using System;

//sync physics property to ON
//set the Process Mode property of the AnimationPlayer to “Physics”

public class Platform : Node2D
{
  AnimationPlayer Anim;

	public override void _Ready()
	{
		Anim = GetNode<AnimationPlayer>("Anim");
		Anim.Play("Horizontal");
	}
}
