using Godot;
using System;

public class LevelDoor : Area2D
{
	[Export]
	public Level.WorldLevel Destination;

	[Export]
	public string DoorText = "";

	[Export]
	public string RequiredKey = "";

	[Signal]
	public delegate void Activated(Level.WorldLevel dest);

	public override void _Ready()
	{
		InputEvent ev = (InputEvent)InputMap.GetActionList("Activate")[0];
		GetNode<Label>("LblActivate").Text = "[" + ev.AsText() + "] to activate" + (RequiredKey == "" ? "" : "\n" + "Requires the " + RequiredKey + " Key");

		GetNode<Label>("LblFlavour").Text = "➞ " + DoorText;
	}

	public override void _Input(InputEvent ev)
	{
		base._Input(ev);

		if (ev.IsActionPressed("Activate"))
		{
			foreach (object body in GetOverlappingBodies())
			{
				if (body is Player player)
				{
					if (RequiredKey != "")
					{
						// GD.Print(player.Keys);
						if (!player.Keys.Contains(RequiredKey))
						{
							return;
							// animation requires blah key ?
						}
					}

					if (!GetNode<AnimationPlayer>("Sprite/Anim").IsPlaying())
					{
						GetNode<AudioData>("AudioData").StartPlaying = true;
						EmitSignal(nameof(LevelDoor.Activated), Destination);
					}
				}
			}
		}
	}

	private void OnLevelDoorBodyEntered(object body)
	{
		if (body is Player player && !GetNode<AnimationPlayer>("Anim").IsPlaying())
		{
			GetNode<AnimationPlayer>("Anim").Play("Hover");
		}
	}

}


