using Godot;
using System;

public class Knife : Area2D
{
	private float _timeTravelled = 0;
	private float _speed = 30f;
	private Vector2 _currentVel = new Vector2(0,0);

	public float Damage {get; set;} = 10f;

	private bool _willDieFromBody = false;

	private Position2D _directionMarker;
	public override void _Ready()
	{
		_directionMarker = GetNode<Position2D>("DirectionMarker");

		// Testing
		// Init(90);
	}

	public void Init(float rotationDegrees)
	{
		_directionMarker.RotationDegrees = rotationDegrees;
		_currentVel = _currentVel + new Vector2(0, - _speed).Rotated(_directionMarker.Rotation);
	}

	public override void _PhysicsProcess(float delta)
	{
		Position += _currentVel;
		_timeTravelled += delta;
		// Kill after distance (off the screen by some margin)
		if (_timeTravelled > 2 && !_willDieFromBody)
		{
			QueueFree();
		}
	}

	// Kill on entering any Physics body after a short delay (allows for other methods to do things with the knife. The delay may not be necessary)
	private void OnKnifeBodyEntered(object body)
	{
		// bodies to ignore...
		if (body is Player player)
		{
			return;
		}

		if (body is Enemy enemy)
		{
			// hack because sarah cant fix her enemy dying repeatedly bug
			if (enemy.GetState() == Enemy.State.Dying)
			{
				return;
			}
			enemy.TakeDamage(Damage);
		}


		_willDieFromBody = true; // So we don't kill it twice...

		// Consider playing a death animation for the knife BEFORE disappearing


		// Visible = false;
		// await ToSignal(GetTree().CreateTimer(3), "timeout");
		QueueFree();
		// Replace with function body.
	}

}
