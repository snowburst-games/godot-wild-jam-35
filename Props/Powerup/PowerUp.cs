using Godot;
using System;

public class PowerUp : Area2D
{
	public override void _Ready()
	{
		
	}

	private bool _activated = false;

	public enum PowerUpType { Damage, Health, Speed, Weapon, HealthBig, MaxHealth}

	[Export]
	public PowerUpType CurrentPowerUpType = PowerUpType.Health;


	[Signal]
	public delegate void AnnounceActivated(string text, Vector2 location);

	private async void OnPowerUpBodyEntered(object body)
	{
		if (body is Player player)
		{
			if (_activated)
			{
				return;
			}
			_activated = true;
			string announceText = "";
			switch (CurrentPowerUpType)
			{
				case PowerUpType.Damage:
					player.BonusDamage += 1f;
					announceText = "+1 Damage";
					break;
				case PowerUpType.Health:
					player.SubtractFromHealth(-1);
					announceText = String.Format("+{0} Health", 1);
					break;
				case PowerUpType.Speed:
					player.BonusSpeed += 1f;
					announceText = "+1 Speed";
					break;
				case PowerUpType.Weapon:
					player.ActivateWeapon(true);
					announceText = "The Blade!";
					break;
				case PowerUpType.HealthBig:
					player.SubtractFromHealth(-(float)Math.Ceiling(player.MaximumHealth/2f));
					announceText = String.Format("+{0} Health", (float)Math.Ceiling(player.MaximumHealth/2f));
					break;
				case PowerUpType.MaxHealth:
					player.ModMaxHealth(1);
					announceText = String.Format("+1 Maximum Health");
					break;
			}
			EmitSignal(nameof(AnnounceActivated), announceText, Position);//GetGlobalTransformWithCanvas().origin);
			GetNode<AnimationPlayer>("Anim").Play("Die");
			await ToSignal(GetNode<AnimationPlayer>("Anim"), "animation_finished");
			QueueFree();
		}
	}

	

}
