using Godot;
using System;

public class Hint : Area2D
{
	[Export]
	public bool AddToJournal {get; set;}= false;

	[Export(PropertyHint.MultilineText)]
	public string HintText {get; set;}= "Untitled hint";

	[Export]
	public string AudioPath {get; set;} = "";

	[Signal]
	public delegate void JournalHintFound(string _hintText);

	private bool _activated = false;

	[Export]
	public int Instructions {get; set;} = 0; // 1 = dir, 2 = attack, 3 = menu

	[Export]
	public string SpritePath {get; set;} = "";

	public override void _Ready()
	{	
		string up = ((InputEvent)InputMap.GetActionList("Up")[0]).AsText();
		string down = ((InputEvent)InputMap.GetActionList("Down")[0]).AsText();
		string left = ((InputEvent)InputMap.GetActionList("Left")[0]).AsText();
		string right = ((InputEvent)InputMap.GetActionList("Right")[0]).AsText();
		string attack = ((InputEvent)InputMap.GetActionList("Attack")[0]).AsText();
		string pause = ((InputEvent)InputMap.GetActionList("Pause")[0]).AsText();
		string jump = ((InputEvent)InputMap.GetActionList("Jump")[0]).AsText();
		string journal = ((InputEvent)InputMap.GetActionList("Journal")[0]).AsText();
		string activate = ((InputEvent)InputMap.GetActionList("Activate")[0]).AsText();
		if (Instructions == 1)
		{ // up down left right attack pause jump journal activate
			HintText = "[" + left + "]" + "and [" + right + "]" + " to move.\n[" + jump + "]" + " to jump.\n[" + up + "] to look up. [" + down + "] to crouch.\n[" + activate + "]" + " to activate gates.";
			GetNode<AudioData>("AudioData").Streams.Clear();
			GetNode<Sprite>("CntSprite/Sprite").Visible = false;
		}
		else if (Instructions == 2)
		{
			HintText = "[" + attack + "]" + " to attack.\nMust be wielding a weapon.";
		}
		else if (Instructions == 3)
		{
			HintText = "[" + pause + "]" + " to pause and bring up the menu.\n[" + journal + "]" + " to show your journal.";
		}
		Label lbl = GetNode<Label>("Label");
		lbl.Text = HintText;

		if (AudioPath != "")
		{
			GetNode<AudioData>("NarrationAudioData").Streams.Add(GD.Load<AudioStreamSample>(AudioPath));
		}

		if (SpritePath != "")
		{
			GetNode<Sprite>("CntSprite/Sprite").Texture = GD.Load<Texture>(SpritePath);
			if (SpritePath == "res://Props/Hint/Sprites/Graveyard_v1.png")
			{
				GetNode<CollisionShape2D>("GraveyardShape").Disabled = false;
			}
			if (SpritePath == "res://Props/Hint/Sprites/bird_carrying_scroll_v1.png" || SpritePath == "res://Props/Hint/Sprites/bird_v1.png")
			{
				GetNode<AnimationPlayer>("AnimBird").Play("Fly");
			}
		}
	}

	private async void OnHintBodyEntered(PhysicsBody2D body)
	{
		if (body is Player player)
		{
			if (_activated)
			{
				return;
			}
			_activated = true;
			GetNode<AnimationPlayer>("Anim").Play("ShowHint");
			GetNode<AudioData>("NarrationAudioData").StartPlaying = true;
			if (AddToJournal)
			{
				EmitSignal(nameof(JournalHintFound), HintText);
				AddToJournal = false;
			}

			await ToSignal(GetNode<AnimationPlayer>("Anim"), "animation_finished");
			QueueFree();
		}


	}


}
