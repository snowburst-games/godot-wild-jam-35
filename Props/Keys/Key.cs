using Godot;
using System;

public class Key : Area2D
{

	private bool _dying = false;

	[Export]
	public string KeyText = "";

	[Signal]
	public delegate void AnnounceActivated(string text, Vector2 location);

	[Signal]
	public delegate void JournalHintFound(string _hintText);

	public override void _Ready()
	{
		_dying = false;
	}

	private async void OnKeyBodyEntered(object body)
	{
		if (KeyText == "")
		{
			GD.Print("Error key is empty at " + Position);
			throw new Exception();
		}
		if (body is Player player)
		{
			if (_dying)
			{
				return;
			}
			_dying = true;
			EmitSignal(nameof(AnnounceActivated), KeyText + " Key Found!", Position);//GetGlobalTransformWithCanvas().origin);
			EmitSignal(nameof(JournalHintFound), KeyText + " Key Found!");
			// GD.Print("how many times is this run?");
			GetNode<AnimationPlayer>("Anim").Play("Die");
			if (!player.Keys.Contains(KeyText))
			{
				player.Keys.Add(KeyText);
			}
			await ToSignal(GetNode<AnimationPlayer>("Anim"), "animation_finished");

			QueueFree();
		}
	}

}
